# Aviarc Lint (avlint)

A tool which helps to detect errors and potential problems in your Aviarc XML code.

## Installation

1. Install node and npm
1. Clone this repo
1. cd into the cloned folder and run ```npm install```

## Run

Execute:

```node src/main.js --path /path/tp/project/folder```

You may use ```-p``` as a shorthand for ```--path```.
The path must be to the top-level folder of the project (where "widgets", "screens", etc live).
If your path points to a sybmbolic link, ensure your path ends with a slash.

## System Architecture

Packages:

* **file-system** Walks the provided path and identifies all xml to be analysed.
* **aviarc-file** Parses files and associates file with Aviarc types such as: "widget-definition", "widget-template", "screen", etc.
* **lint** Analsyses each file and avaluates rules against them.
* **reporter** Different methods of outputing validation results from the lint package.
* **settings** Loads and parses settings files (```.avlintrc```).


## Useful links

* https://github.com/jshint/jshint-next/wiki/Design
* http://jshint.com/docs/options/
* https://github.com/jindw/xmldom
* https://github.com/jshint/jshint/blob/2.1.4/src/shared/messages.js
* http://jslinterrors.com
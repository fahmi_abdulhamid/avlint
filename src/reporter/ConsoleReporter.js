'use strict';

var colors = require('colors/safe');

function report(code, reason, evidence, path, lineNumber, columnNumber) {
    var descriptionLine = path + ' line ' + lineNumber + ' col ' + columnNumber;
    var reasonLine = '\t' + reason;
    var evidenceLine =  '\t' + evidence;
    var emptyLine = '';

    var result = [
        colors.bold(descriptionLine),
        reasonLine,
        colors.grey(evidenceLine),
        emptyLine].join('\n');
    console.log(result);
}

exports.report = report;

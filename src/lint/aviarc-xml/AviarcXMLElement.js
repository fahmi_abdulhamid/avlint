'use strict';

function XMLElement(name, prefix, namespace, isSubelement, filePosition) {
    this.name = name;
    this.prefix = prefix;
    this.namespace = namespace;
    this.isSubelement = isSubelement;
    this.filePosition = filePosition;

    this.attributes = [];
    this.children = [];
}

function addAttribute(xmlElement, name, value) {
    xmlElement.attributes.push({name: name, value: value});
}

function addChild(xmlElement, childElement) {
    xmlElement.children.push(childElement);
}

function FilePosition(lineNumber, columnNumber) {
    this.lineNumber = lineNumber;
    this.columnNumber = columnNumber;
}

exports.XMLElement = XMLElement;
exports.FilePosition = FilePosition;
exports.addAttribute = addAttribute;
exports.addChild = addChild;

'use strict';

var detect = require('detect-line-endings');

var XML_START_REGEX = /</;
var COMMENT_START_REGEX = /<!--/;
var COMMENT_END_REGEX = /-->/;

/**
 * Creates a map of line endings between those produced by the
 * xmldoc library and those in the provided XML source.
 *
 * xmldoc's line endings are taken after multi-line XML elements are
 * joined and empty lines are removed.
 */
function makeLineNumberMap(xmlString, xmlDoc) {
    var lineEnding = detect(xmlString).EOL;
    var xmlLines = xmlString.split(lineEnding);

    var lineNumberMap = {};
    var elementQueue = [xmlDoc.firstChild];
    while(elementQueue.length > 0) {
        var element = elementQueue.shift();

        var parsedLineNumber = element.lineNumber;
        var sourceLineNumber = getSourceLineNumber(xmlLines, element);

        lineNumberMap[parsedLineNumber] = sourceLineNumber;

        elementQueue.push.apply(elementQueue, element.childNodes);
    }

    return lineNumberMap;
}

function getSourceLineNumber(xmlLines, element) {
    var parsedLineNumber = element.lineNumber;

    var lineNumInParsedXML = 0;
    var lineNumInSourceFile = 0;
    var inComment = false;
    for(; lineNumInSourceFile < xmlLines.length && lineNumInParsedXML < parsedLineNumber; lineNumInSourceFile++) {
        var line = xmlLines[lineNumInSourceFile];

        if(!inComment && COMMENT_START_REGEX.test(line)) {
            inComment = true;
        }

        if(inComment && COMMENT_END_REGEX.test(line)) {
            inComment = false;
        }

        if(inComment) {
            // All lines in comments are accounted for
            lineNumInParsedXML += 1;
        } else if ((line.length > 0 && line.trim().length === 0) || XML_START_REGEX.test(line)) {
            // Only lines with element start tags and whitespace-only lines are accounted for
            lineNumInParsedXML += 1;
        }
    }

    return lineNumInSourceFile;
}

exports.makeLineNumberMap = makeLineNumberMap;

'use strict';

function iterate(xmlElementRoot, iterFunc) {
    var elementQueue = [xmlElementRoot];
    while(elementQueue.length > 0) {
        var xmlElement = elementQueue.shift();

        iterFunc(xmlElement);

        var childElements = xmlElement.children;
        elementQueue.push.apply(elementQueue, childElements);
    }

}

exports.iterate = iterate;

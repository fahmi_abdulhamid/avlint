'use strict';

var XMLUtils = require('./XMLUtils');
var AviarcTypes = require('../../aviarc-file/AviarcTypes');

var DEFAULT_WIDGET_NAMESPACE = 'urn:aviarc:widget:com.aviarc.toronto.widget.core';
//var DEFAULT_COMMAND_NAMESPACE = 'com.aviarc.base';

function getDefaultNamespace(aviarcXMLFile) {
    var type = aviarcXMLFile.getType();
    var subtype = aviarcXMLFile.getSubtype();

    var defaultNamespace = null;
    if(type === AviarcTypes.Type.SCREEN) {
        defaultNamespace = DEFAULT_WIDGET_NAMESPACE;
    }else if(type === AviarcTypes.Type.WIDGET && subtype === AviarcTypes.Subtype.TEMPLATE) {
        defaultNamespace = DEFAULT_WIDGET_NAMESPACE;
    }

    return defaultNamespace;
}

function updateNamespace(xmlElement, defaultNamespace) {
    if(!xmlElement.namespace) {
        xmlElement.namespace = defaultNamespace;
    }
}

function identify(aviarcXMLFile, xmlElementRoot) {
    var defaultNamespace = getDefaultNamespace(aviarcXMLFile);

    if(defaultNamespace) {
        XMLUtils.iterate(xmlElementRoot, function(xmlElement) {
            updateNamespace(xmlElement, defaultNamespace);
        });
    }
}

exports.identify = identify;

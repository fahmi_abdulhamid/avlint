'use strict';

function isNotSubelement(xmlElement) {
    return !xmlElement.isSubelement;
}

function getMatchingNamespaceRule(subelementRules, namespace) {
    var availableNamespaces = Object.keys(subelementRules);
    var matchingNamespaces = availableNamespaces.filter(function(currentNamespace) {
        return currentNamespace.indexOf(namespace) === 0;
    });

    var firstMatchingNamespace = matchingNamespaces[0];
    var matchingSubelementRule = subelementRules[firstMatchingNamespace] || {};
    return matchingSubelementRule;
}

function markElementWithRule(xmlElement, namespace, rule) {
    var matchedRule = rule[xmlElement.name];
    if(matchedRule) {
        xmlElement.isSubelement = true;

        // Subelements in Aviarc XML don't have to be namespaced.
        // Subelements must have their namespaces associated with their parent.
        xmlElement.namespace = namespace;

        markChildrenWithRule(xmlElement, namespace, matchedRule);
    }
}

function markChildrenWithRule(xmlElement, namespace, rule) {
    xmlElement.children.forEach(function(childXMLElement) {
        markElementWithRule(childXMLElement, namespace, rule);
    });
}

function identify(xmlElementRoot, subelementRules) {
    var elementQueue = [xmlElementRoot];
    while(elementQueue.length > 0) {
        var xmlElement = elementQueue.shift();
        var namespace = xmlElement.namespace;

        var rule = getMatchingNamespaceRule(subelementRules, xmlElement.namespace);
        markElementWithRule(xmlElement, namespace, rule);

        var childElements = xmlElement.children
            .filter(isNotSubelement);
        
        elementQueue.push.apply(elementQueue, childElements);
    }

}

exports.identify = identify;

'use strict';

var AviarcXMLElement = require('./AviarcXMLElement');
var XMLDOMMapping = require('./XMLDOMMapping');

function addAttributes(node, xmlElement) {
    var numAttributes = node.attributes ? node.attributes.length : 0;
    for(var i = 0; i < numAttributes; i++) {
        var attribute = node.attributes.item(i);
        AviarcXMLElement.addAttribute(xmlElement, attribute.name, attribute.value);
    }
}

function addChildren(lineNumberMap, node, xmlElement) {
    var numChildNodes = node.childNodes ? node.childNodes.length : 0;
    for(var i = 0; i < numChildNodes; i++) {
        var childNode = node.childNodes.item(i);

        if(childNode.nodeType === childNode.ELEMENT_NODE) {
            AviarcXMLElement.addChild(xmlElement, buildXMLElement(lineNumberMap, childNode));
        }
    }
}

function buildXMLElement(lineNumberMap, node) {
    var name = node.localName;
    var prefix = node.prefix;
    var namespace = node.namespaceURI || '';
    var isSubelement = false;

    var lineNumber = lineNumberMap[node.lineNumber];
    var columnNumber = node.columnNumber;
    var filePosition = new AviarcXMLElement.FilePosition(lineNumber, columnNumber);

    var xmlElement =  new AviarcXMLElement.XMLElement(name, prefix, namespace, isSubelement, filePosition);

    addAttributes(node, xmlElement);
    addChildren(lineNumberMap, node, xmlElement);

    return xmlElement;
}

function buildXMLTree(aviarcXMLFile) {
    var xmlDoc = aviarcXMLFile.getXMLDoc();
    var lineNumberMap = XMLDOMMapping.makeLineNumberMap(aviarcXMLFile.getXMLString(), xmlDoc);

    return buildXMLElement(lineNumberMap, xmlDoc.firstChild);
}

exports.buildXMLTree = buildXMLTree;

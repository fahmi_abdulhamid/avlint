'use strict';

var Rules = {
    widget: {
        template: [
            require('./widget-rules/NamePrefix'),
            require('./general-rules/UniqueNamespaces')
        ],
        definition: [
            require('./general-rules/MandatoryDefaultAttr')
        ]
    },
    screen: {
        none: []
    }
};

module.exports = Rules;

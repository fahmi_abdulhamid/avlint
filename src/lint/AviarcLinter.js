'use strict';

var detect = require('detect-line-endings');

var Rules = require('./Rules');

var AviarcXMLBuilder = require('./aviarc-xml/AviarcXMLBuilder');
var SubelementIdentifier = require('./aviarc-xml/SubelementIdentifier');
var DefaultNamespaceIdentifier = require('./aviarc-xml/DefaultNamespaceIdentifier');

function getEvidence(xmlLines, lineNumber) {
    return xmlLines[lineNumber - 1];
}

function reportWarnings(reporter, aviarcXMLFile, aviarcXMLElement, warnings) {
    var xmlString = aviarcXMLFile.getXMLString();
    var lineEnding = detect(xmlString).EOL;
    var xmlLines = xmlString.split(lineEnding);

    warnings.forEach(function(warning) {
        var lineNumber = aviarcXMLElement.filePosition.lineNumber;
        var columnNumber = aviarcXMLElement.filePosition.columnNumber;
        var evidence = getEvidence(xmlLines, lineNumber);
        reporter.report(warning.code, warning.reason, evidence, aviarcXMLFile.getPath(), lineNumber, columnNumber);
    });
}

function processXMLElement(reporter, aviarcXMLFile, aviarcXMLElement) {
    var rules = Rules[aviarcXMLFile.getType()][aviarcXMLFile.getSubtype()] || [];
    rules
        .map(function(rule) {
            return rule.visit(aviarcXMLElement);
        })
        .forEach(function(warnings) {
            reportWarnings(reporter, aviarcXMLFile, aviarcXMLElement, warnings);
        });
}

function process(reporter, aviarcXMLFile, settings) {
    var xmlDoc = aviarcXMLFile.getXMLDoc();
    var aviarcXMLRoot = AviarcXMLBuilder.buildXMLTree(aviarcXMLFile);

    DefaultNamespaceIdentifier.identify(aviarcXMLFile, aviarcXMLRoot);
    SubelementIdentifier.identify(aviarcXMLRoot, settings['sub-elements']);

    // Iterate over both trees.
    // The tree of DOM nodes is used for parsing hints in comments
    // The tree of Aviarc XML elements is used for evaluating rules
    var nodeQueue = [xmlDoc.firstChild];
    var aviarcXMLQueue = [aviarcXMLRoot];
    while(nodeQueue.length > 0) {
        var node = nodeQueue.shift();
        
        if(node.nodeType === node.ELEMENT_NODE) {
            var aviarcXMLElement = aviarcXMLQueue.shift();

            processXMLElement(reporter, aviarcXMLFile, aviarcXMLElement);

            if(node.childNodes) {
                nodeQueue.push.apply(nodeQueue, node.childNodes);
                aviarcXMLQueue.push.apply(aviarcXMLQueue, aviarcXMLElement.children);
            }
        }

    }
}


exports.process = process;

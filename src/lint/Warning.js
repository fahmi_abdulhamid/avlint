'use strict';

function Warning(code, reason) {
    this.code = code;
    this.reason = reason;
}

module.exports = Warning;

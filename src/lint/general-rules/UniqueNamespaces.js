'use strict';

var Warning = require('../Warning');
var Util = require('../Util');

function isNamespaceAttribute(attribute) {
    return attribute.name.indexOf('xmlns') === 0;
}

function getAttributeValue(attribute) {
    return attribute.value;
}

function visit(xmlElement) {
    var namespaces = xmlElement.attributes
        .filter(isNamespaceAttribute)
        .map(getAttributeValue);

    var uniqueNamespaces = Util.unique(namespaces);

    var warnings = [];

    if(namespaces.length !== uniqueNamespaces.length) {
        var warning = new Warning('unique_namespaces', 'Duplicate namespace declared');
        warnings.push(warning);
    }

    return warnings;
}

exports.visit = visit;

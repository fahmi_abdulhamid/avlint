'use strict';

var Warning = require('../Warning');
var Util = require('../Util');

function isMandatory(attribute) {
    return attribute.name === 'mandatory' && Util.toBoolean(attribute.value);
}

function hasDefaultValue(attribute) {
    return attribute.name === 'default';
}

function hasMandatoryAndDefaultValue(attributes) {
    return attributes.some(isMandatory) && attributes.some(hasDefaultValue);
}

function visit(xmlElement) {
    var warnings = [];

    if(xmlElement.name === 'attribute' && hasMandatoryAndDefaultValue(xmlElement.attributes)) {
        var warning = new Warning('mandatory_default_attr', 'Mandatory attribute declared with a default value');
        warnings.push(warning);
    }

    return warnings;
}

exports.visit = visit;

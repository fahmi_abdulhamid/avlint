'use strict';

function exists(value) {
    return value != null;
}

function unique(values) {
    return values.filter(function(value, index, values) {
        return values.indexOf(value) === index;
    });
}

function toBoolean(aviarcBoolean) {
    var normalisedAviarcBoolean = String(aviarcBoolean).trim().toLowerCase();
    return ['true', 't', 'yes', 'y', '1'].indexOf(normalisedAviarcBoolean) !== -1;
}

exports.exists = exists;
exports.unique = unique;
exports.toBoolean = toBoolean;

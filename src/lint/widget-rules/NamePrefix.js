'use strict';

var Warning = require('../Warning');
var Util = require('../Util');

function checkAttribute(attribute) {
    if (attribute.name === 'name' && attribute.value.indexOf('{@name}') !== 0) {
        return new Warning('widget_name_prefix', 'Widget name not prefixed with \'{@name}\'');
    }
}

function visit(xmlElement) {
    if (xmlElement.namespace.indexOf('urn:aviarc:widget:com.aviarc.toronto.widget.core.') !== -1 ||
        xmlElement.isSubelement) {
        return [];
    }

    return xmlElement.attributes
        .map(checkAttribute)
        .filter(Util.exists);
}

exports.visit = visit;

'use strict';

var readFile = require('fs-readfile-promise');

function parseFileBuffer(buffer) {
    var settingsString = buffer.toString();
    return JSON.parse(settingsString);
}

function load(settingsPath) {
    return readFile(settingsPath)
        .then(parseFileBuffer);
}

exports.load = load;

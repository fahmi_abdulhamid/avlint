'use strict';

var AviarcTypes = require('./AviarcTypes');

var getType = function(rootDir, fileName) {
    var type = rootDir === 'screens' ? AviarcTypes.Type.SCREEN : null;
    return type;
};

var getSubtype = function(fileName, xmlDoc) {
    return null;
};

module.exports.getType = getType;
module.exports.getSubtype = getSubtype;


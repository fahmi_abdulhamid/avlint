'use strict';

var path = require('path');
var readFile = require('fs-readfile-promise');
var DOMParser = require('xmldom').DOMParser;

var AviarcXMLFile = require('./AviarcXMLFile');
var AviarcTypes = require('./AviarcTypes');

var AviarcXMLFileLoader = function() {
    this._validators = [
        //require('./XMLCommandValidator'),
        require('./WidgetValidator'),
        require('./ScreenValidator'),
        //require('./DatabrokerWorkflowValidator'),
        //require('./DataruleWorkflowValidator'),
        //require('./DataruleActionWidgetValidator'),
    ];
};

AviarcXMLFileLoader.prototype = {
    load: function(appDir, filePath) {
        var fullPath = path.join(appDir, filePath);
        return readFile(fullPath)
            .then(this._createFileObject.bind(this, filePath));
    },

    _createFileObject: function(filePath, buffer) {
        var rootDir = filePath.split(path.sep)[0];
        var fileName = path.basename(filePath);
        var xmlString = buffer.toString();
        var xmlDoc = this._parseXMLFile(xmlString);

        var validAviarcFiles = this._validators
            .map(this._validateType.bind(this, rootDir, fileName, filePath, xmlDoc, xmlString))
            .filter(this._isValidFileType);

        var aviarcFile = validAviarcFiles[0];
        return aviarcFile;
    },

    _validateType: function(rootDir, fileName, filePath, xmlDoc, xmlString, xmlFileValidator) {
        var type = xmlFileValidator.getType(rootDir, fileName);
        if(!type) {
            type = AviarcTypes.Type.UNKNOWN;
        }

        var subtype = AviarcTypes.Subtype.NONE;
        if(type !== AviarcTypes.Type.UNKNOWN) {
            var retrievedSubtype = xmlFileValidator.getSubtype(fileName, xmlDoc);
            if(retrievedSubtype) {
                subtype = retrievedSubtype;
            }
        }

        return new AviarcXMLFile(filePath, type, subtype, xmlString, xmlDoc);
    },

    _isValidFileType: function(aviarcXMLFile) {
        return aviarcXMLFile.getType() !== AviarcTypes.Type.UNKNOWN;
    },

    _parseXMLFile: function(xmlString) {
        var domParser = new DOMParser();
        return domParser.parseFromString(xmlString, 'text/xml');
    }
};

module.exports = AviarcXMLFileLoader;

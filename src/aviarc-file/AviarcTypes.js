'use strict';

var AviarcTypes = {
    UNKNOWN: 'unknown',
    SCREEN: 'screen',
    WIDGET: 'widget'
};

var AviarcSubtypes = {
    NONE: 'none',
    DEFINITION: 'definition',
    TEMPLATE: 'template'
};

module.exports.Type = AviarcTypes;
module.exports.Subtype= AviarcSubtypes;


'use strict';

var AviarcXMLFile = function(path, type, subtype, xmlString, xmlDoc) {
    this._path = path;
    this._type = type;
    this._subtype = subtype;
    this._xmlString = xmlString;
    this._xmlDoc = xmlDoc;
};

AviarcXMLFile.TYPE_UNKNOWN = 'type_unknown';

AviarcXMLFile.prototype = {
    getPath: function() {
        return this._path;
    },

    getType: function() {
        return this._type;
    },

    getSubtype: function() {
        return this._subtype;
    },

    getXMLDoc: function() {
        return this._xmlDoc;
    },

    getXMLString: function() {
        return this._xmlString;
    },

    toString: function() {
        return 'AviarcXMLFile: ' + this.getPath() +
            '\n\ttype: '+ this.getType() +
            '\n\tsubtype: '+ this.getSubtype();
    }
};

module.exports = AviarcXMLFile;

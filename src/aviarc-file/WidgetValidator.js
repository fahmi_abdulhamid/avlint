'use strict';

var AviarcTypes = require('./AviarcTypes');

var getType = function(rootDir, fileName) {
    var type = rootDir === 'widgets' ? AviarcTypes.Type.WIDGET : null;
    return type;
};

var getSubtype = function(fileName, xmlDoc) {
    var subtype = fileName === 'definition.xml' ? 
        AviarcTypes.Subtype.DEFINITION : 
        AviarcTypes.Subtype.TEMPLATE;

    return subtype;
};

module.exports.getType = getType;
module.exports.getSubtype = getSubtype;


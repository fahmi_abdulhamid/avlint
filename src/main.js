'use strict';

global.Promise = require('bluebird');

var commandLineArgs = require('command-line-args');

var AviarcAppDirWalker = require('./file-system/AviarcAppDirWalker');
var AviarcXMLFileLoader = require('./aviarc-file/AviarcXMLFileLoader');
var SettingsLoader = require('./settings/SettingsLoader');
var AviarcLinter = require('./lint/AviarcLinter');
var ConsoleReporter = require('./reporter/ConsoleReporter');


var cli = commandLineArgs([
    { name: 'version', type: Boolean },
    { name: 'help', alias: 'h', type: Boolean },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'path', alias: 'p', type: String, defaultValue: '.'}
]);

var applicationDescription = {
    title: 'Aviarc Lint (avlint)',
    description: 'Helps to detect errors and potential problems in your Aviarc XML code'
};

var options;

try {
    options = cli.parse();
} catch (er) {
    console.error(er.message);
    console.log(cli.getUsage(applicationDescription));
    process.exit(1);
}

console.log('Arguments:');
console.log(JSON.stringify(options, null, 4));

if(options.version) {
    console.log(process.env.npm_package_version || 'development');
    process.exit(0);
}

if(options.help) {
    console.log(cli.getUsage(applicationDescription));
    process.exit(0);
}

var path = options.path;

function processFiles(args) {
    var files = args[0];
    var settings = args[1];
    console.log(' ' + files.length + ' files found');

    var aviarcXMLFileLoader = new AviarcXMLFileLoader();
    files.forEach(function(file) {
        aviarcXMLFileLoader.load(path, file)
            .then(function(aviarcXMLFile) {
                if(aviarcXMLFile) {
                    AviarcLinter.process(ConsoleReporter, aviarcXMLFile, settings);
                }
            });
    });
}

function getFiles() {
    console.log('Walking project...');

    var aviarcAppDirWalker = new AviarcAppDirWalker();
    return aviarcAppDirWalker.getFiles(path);
}

function loadSettings() {
    var path = './default.avlintrc';
    return SettingsLoader.load(path);
}

Promise.all([
        getFiles(),
        loadSettings()
    ])
    .then(processFiles);


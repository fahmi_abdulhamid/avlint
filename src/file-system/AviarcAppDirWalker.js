'use strict';

var Promise = require('bluebird');
var walk = require('walkdir');
var path = require('path');

/**
 * Walks the directory structure of Aviarc applications to retrieve
 * XML files which correspond to XMLCommands or template widgets.
 *
 * @class
 */
var AviarcAppDirWalker = function() {
};

AviarcAppDirWalker.prototype = {
    DIRS_TO_WALK: [
        //'databrokers', 'datarules', 'screens', 'widgets', 'workflows', 'xmlcommands'
        'widgets'
    ],
    FILES_TO_INCLUDE_REGEX: /.*\.xml$/,
    FILES_TO_EXCLUDE_REGEX: /\/themes?\//, // Takes precedence
    
    getFiles: function(appPath) {
        var DIRS_TO_WALK = this.DIRS_TO_WALK;
        var FILES_TO_INCLUDE_REGEX = this.FILES_TO_INCLUDE_REGEX;
        var FILES_TO_EXCLUDE_REGEX = this.FILES_TO_EXCLUDE_REGEX;

        return new Promise(function(resolve, reject) {
            var files = [];
            var emitter = walk(appPath, function(filePath, stat) {
                var appRelativePath = path.relative(appPath, filePath);

                var appRelativePathParts = appRelativePath.split(path.sep);
                var appRootDir = appRelativePathParts[0];

                if(DIRS_TO_WALK.indexOf(appRootDir) === -1) {
                    this.ignore(filePath);
                }
            });

            emitter.on('file', function(filePath, stat) {
                if(!FILES_TO_EXCLUDE_REGEX.test(filePath) && FILES_TO_INCLUDE_REGEX.test(filePath)) {
                    var appRelativePath = path.relative(appPath, filePath);
                    files.push(appRelativePath);
                }
            });

            emitter.on('end', function() {
                resolve(files);
            });

            emitter.on('error', function(path, error) {
                reject(error.message);
            });

        });
    },
};


module.exports = AviarcAppDirWalker;
